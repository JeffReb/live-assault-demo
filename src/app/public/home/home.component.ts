import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Settings } from '../../app.settings.model';
import { AppSettings } from '../../app.settings';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  public settings: Settings;

  constructor(public appSettings: AppSettings) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
    this.settings.rtl = false;
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }

  public scrollToDemos() {
    setTimeout(() => {
      window.scrollTo(0, 520);
    });
  }

  public changeLayout(menu, menuType, isRtl) {
    this.settings.menu = menu;
    this.settings.menuType = menuType;
    this.settings.rtl = isRtl;
    this.settings.theme = 'indigo-light';
  }

  public changeTheme(theme) {
    this.settings.theme = theme;
  }
}
