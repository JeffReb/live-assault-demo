import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ErrorComponent } from './public/errors/error/error.component';
import { NotFoundComponent } from './public/errors/not-found/not-found.component';

export const routes: Routes = [
  { path: '', loadChildren: () => import('./public/home/home.module').then(mode => mode.HomeModule) },
  { path: 'login', loadChildren: () => import('./public/login/login.module').then(mode => mode.LoginModule) },
  {
    path: 'register',
    loadChildren: () => import('./public/register/register.module').then(mode => mode.RegisterModule)
  },
  { path: 'error', component: ErrorComponent, data: { breadcrumb: 'Error' } },
  { path: '**', component: NotFoundComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
  // preloadingStrategy: PreloadAllModules,  // <- uncomment this line for disable lazy load
  // useHash: true
});
