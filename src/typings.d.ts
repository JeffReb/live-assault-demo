interface Document {
  fullscreenElement: Element;
  msFullscreenElement: Element;
  webkitFullscreenElement: Element;
  exitFullscreen(): void;
  msExitFullscreen(): void;
  mozCancelFullScreen(): void;
  mozFullScreenElement(): void;
  webkitExitFullscreen(): void;
  webkitCancelFullScreen(): void;
}
